#define BLYNK_PRINT Serial

#include<ESP8266WiFi.h>
#include<BlynkSimpleEsp8266.h>

char auth[] = "fb14a099b12d43839ab32d1e8c1ad57a";

// Your WiFi credentials.
// Set password to "" for open networks.
char ssid[] = "DESKTOP-Rahul";
char pass[] = "rahul404";
int pinP=A0;
int x = 0;
int LastTime = 0;
bool BPMTiming = false;
bool BeatComplete = false;
bool IgnoreReading=false;
bool FirstPulseDetected = false;
    unsigned long FirstPulseTime = 0;
    unsigned long SecondPulseTime = 0;
    unsigned long PulseInterval = 0;
int BPM = 0;    
#define UpperThreshold 518
#define LowerThreshold 490    

void setup() {
  
  Blynk.begin(auth, ssid, pass);
  
  pinMode(pinP,INPUT);// TO READ THE PULSE RATE DATA
  //pinMode(BodyTemp,INPUT);// TO READ THE BODY TEMP.
 
  Serial.begin(9600);
  
}

float P_Data;
void loop() {
 //Reading analog pulse data
 P_Data=analogRead(pinP);
 int reading=P_Data;
 //************************************************
if(reading > UpperThreshold && IgnoreReading == false){
        if(FirstPulseDetected == false){
          FirstPulseTime = millis();
          FirstPulseDetected = true;
        }
        else{
          SecondPulseTime = millis();
          PulseInterval = SecondPulseTime - FirstPulseTime;
          FirstPulseTime = SecondPulseTime;
        }
        IgnoreReading = true;
      }

      // Heart beat trailing edge detected.
      if(reading < LowerThreshold){
        IgnoreReading = false;
      }  

      BPM = (1.0/PulseInterval) * 60.0 * 1000;

      Serial.print(reading);
      Serial.print("\t");
      Serial.print(PulseInterval);
      Serial.print("\t");
      Serial.print(BPM);
      Serial.println(" BPM");
      Serial.flush();

      //
  //***************************
 //Serial.print("patient Pulse Rate :");
 //Serial.print(P_Data);
 //Serial.print(" beats per minute");
 Serial.println();
 
 Blynk.run();
  if(P_Data>590 || P_Data<400)//590 is THvalue
 {
  Blynk.email("techierahul404@gmail.com","alert! Data is abnormal",P_Data);//let suppose the email of doctor
  Blynk.email("suyashgoyal27@gmail.com","alert!");// staff
 }
 
 // Blynk.email("techierahul404@gmail.com","pulse rate data",P_Data); 
 delay(10000); 
}
